import React from 'react';
import { connect, Provider } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ModalVideo from 'react-modal-video';
import Transition from 'react-motion-ui-pack';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import { getAllMovieData_action } from '../../redux/actionCreators/actionCreators';
import CurrentPageToggle from '../CurrentPageToggle/CurrentPageToggle';
import { auth } from '../../firebase/firebase';
import { db } from '../../firebase';
import * as routes from '../../routes';

import '../../../node_modules/react-modal-video/css/modal-video.min.css';
import './moviePage.css';

class _MoviePage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false,
			movieList: null,
			userId: null,
			movieId: window.location.href.split('/id')[1],
		};

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.addToList = this.addToList.bind(this);
		this.removeFromList = this.removeFromList.bind(this);
		this.isMovieInList = this.isMovieInList.bind(this);
	}

	componentDidMount() {
		this.props.getAllMovieData_action(this.state.movieId, null);

		auth.onAuthStateChanged(user => {
			if (!user) {
				const { history } = this.props;
				history.push(routes.SIGN_IN);
			} else {
				this.setState(
					{
						userId: user.uid,
					},
					() => {
						db
							.getMovieList(this.state.userId)
							.then(snapshot => this.setState({ movieList: snapshot.val() }))
							.then(this.isMovieInList());
					}
				);
			}
		});
	}

	isMovieInList() {
		return (
			this.state.movieList != null &&
			this.state.movieList.hasOwnProperty(this.props.movieDataAll.movieData.original_title)
		);
	}

	addToList(userID, movieTitle, movieDataAll) {
		db.addMovieToList(userID, movieTitle, movieDataAll);
		db.getMovieList(this.state.userId).then(snapshot => this.setState({ movieList: snapshot.val() }));
	}

	removeFromList(userID, movieTitle) {
		db.removeMovieFromList(userID, movieTitle);
		db.getMovieList(this.state.userId).then(snapshot => this.setState({ movieList: snapshot.val() }));
	}

	openModal() {
		this.setState({ isOpen: true });
	}

	closeModal() {
		this.setState({ isOpen: false });
	}

	render() {
		return (
			<div className="container-fluid">
				<CurrentPageToggle />

				<Transition
					component={false} // don't use a wrapping component
					enter={{ opacity: 1 }}
					leave={{ opacity: 0 }}
				>
					<div className="row" key="MoviePageKey">
						<div className="col-md-offset-3 col-md-6 col-xs-offset-1 col-xs-10">
							<div className="GridHolder GridHolder--oneCol h-marginT row">
								<div className="Thumbnail Thumbnail--moviePage h-whitePanel h-paddingAll--sm">
									<div className="Movie__trailerHolder">
										{this.props.movieDataAll.movieDataYTkey && (
											<div className="h-height">
												<div
													className="Movie__trailerHolderInner"
													style={{
														background: `url(https://img.youtube.com/vi/${this.props
															.movieDataAll.movieDataYTkey}/hqdefault.jpg)`,
														backgroundSize: 'cover',
													}}
													onClick={this.openModal}
												/>

												<span className="Movie__playBtn" onClick={this.openModal}>
													<span className="Movie__playBtnIco" />
												</span>

												<ModalVideo
													channel="youtube"
													autoplay
													isOpen={this.state.isOpen}
													onClose={this.closeModal}
													videoId={this.props.movieDataAll.movieDataYTkey}
												/>
											</div>
										)}
									</div>

									<div>
										<div className="Info__data">
											<p className="Info__title">
												{this.props.movieDataAll.movieData.original_title}
											</p>
											<div className="Info__voteHolder">
												<span className="Info__voteAverage">
													{this.props.movieDataAll.movieData.vote_average}
													<span
														id="rating"
														className="Info__voteStars glyphicon glyphicon-star"
													/>
												</span>
												<span className="Info__voteAverage h-fontXs">
													By {this.props.movieDataAll.movieData.vote_count}
													&nbsp;votes
												</span>
												<a
													className="Info__voteAverage Info__voteAverage--imdb h-fontSm"
													target="_blank"
													href={`http://www.imdb.com/title/${this.props.movieDataAll.movieData
														.imdb_id}/`}
												/>
											</div>
										</div>

										<div className="Info__meta">
											<span className="Info__releaseDate">
												<span className="Info__calendar glyphicon glyphicon-calendar"> </span>
												{this.props.movieDataAll.movieData.release_date}
											</span>
											<p className="Info__genres">
												{this.props.movieDataAll.movieData.genres &&
													this.props.movieDataAll.movieData.genres.map(
														(item, index, array) =>
															array.length - 1 === index ? (
																<span key={`genres-key-${index}`}>{item.name}</span>
															) : (
																<span key={`genres-key-${index}`}>{item.name}, </span>
															)
													)}
											</p>
										</div>

										<p className="Info__overview">
											Overview: {this.props.movieDataAll.movieData.overview}
										</p>
										<div className="Info__viewMore">
											{this.isMovieInList() ? (
												<a
													className="h-fontSm h-marginR"
													onClick={() =>
														this.removeFromList(
															this.state.userId,
															this.props.movieDataAll.movieData.original_title
														)}
												>
													Remove from list
												</a>
											) : (
												<a
													className="h-fontSm h-marginR"
													onClick={() =>
														this.addToList(
															this.state.userId,
															this.props.movieDataAll.movieData.original_title,
															this.props.movieDataAll
														)}
												>
													Add to list
												</a>
											)}

											<a
												className="h-fontSm"
												target="_blank"
												href={`https://idope.se/torrent-list/${this.props.movieDataAll.movieData
													.original_title}/`}
											>
												Torrent
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Transition>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		movieDataAll: state.movieDataReducer.movieDataAll,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			getAllMovieData_action,
		},
		dispatch
	);
};

const MoviePageWithRouter = withRouter(_MoviePage);
const MoviePageComponent = connect(mapStateToProps, mapDispatchToProps)(MoviePageWithRouter);

export default class MoviePage extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<MoviePageComponent {...this.props} />
			</Provider>
		);
	}
}
