import React from 'react';
import { connect, Provider } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { store } from '../../redux/store';
import { db } from '../../firebase';
import Thumbnail from '../Thumbnail/Thumbnail';
import CurrentPageToggle from '../CurrentPageToggle/CurrentPageToggle';
import * as routes from '../../routes';
import { auth } from '../../firebase/firebase';

class _ListPage extends React.Component {
	state = {
		movieList: [],
		authUserId: null,
	};

	componentDidMount() {
		auth.onAuthStateChanged(user => {
			if (!user) {
				const { history } = this.props;
				history.push(routes.SIGN_IN);
			} else {
				db.getMovieList(user.uid).then(snapshot => this.setState({ movieList: snapshot.val() }));
			}
		});
	}

	render() {
		return (
			<div className="container-fluid">
				<CurrentPageToggle />

				<div className="row h-marginB">
					{this.state.movieList != null ? (
						<div className="col-xs-offset-1 col-xs-10 GridHolder GridHolder--twoCol GridHolder--gap2 h-marginT">
							{Object.entries(this.state.movieList).map((item, index) => (
								<Thumbnail
									movie={item[1].movieDataAll.movieData}
									key={index}
									apiConfig_response={this.props.apiConfig_response}
									apiConfig_imagesSizes={this.props.apiConfig_imagesSizes}
									apiConfig_imagesBaseUrl={this.props.apiConfig_imagesBaseUrl}
								/>
							))}
						</div>
					) : (
						<div className="col-lg-12 text-center">
							<h1>Currently there are no movies in your watchlist</h1>
							<h1>Use "Add to list" button to collect movies</h1>
						</div>
					)}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		apiConfig_response: state.configurationApiReducer.apiConfig_response,
		apiConfig_imagesSizes: state.configurationApiReducer.apiConfig_imagesSizes,
		apiConfig_imagesBaseUrl: state.configurationApiReducer.apiConfig_imagesBaseUrl,
	};
};

const ListWithRouter = withRouter(_ListPage);
const ListPageComponent = connect(mapStateToProps)(ListWithRouter);

export default class ListPage extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<ListPageComponent {...this.props} />
			</Provider>
		);
	}
}
