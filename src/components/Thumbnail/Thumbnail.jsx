import React from 'react';
import { Link } from 'react-router-dom';
import * as routes from '../../routes';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import { getAllMovieData_action } from '../../redux/actionCreators/actionCreators';
import Transition from 'react-motion-ui-pack';
import './thumbnail.css';

class _Thumbnail extends React.Component {
	constructor(props) {
		super(props);
		this.posterSrc = this.posterSrc.bind(this);
		this.posterImage = this.posterImage.bind(this);
	}

	//this is how we build image src
	posterSrc() {
		let apiConfig_imagesBaseUrl = this.props.apiConfig_imagesBaseUrl;
		let apiConfig_imagesSizes = this.props.apiConfig_imagesSizes[0];
		let movie_poster = this.props.movie.poster_path;
		return apiConfig_imagesBaseUrl + apiConfig_imagesSizes + movie_poster;
	}

	//if api return poster image show that <img> ,if not show no image <div>
	posterImage() {
		if (this.props.movie && this.props.movie.poster_path) {
			return <img className="Poster__img" src={this.posterSrc()} alt={this.props.movie.original_title} />;
		} else {
			return <div className="Poster__img Poster__img--NoImage glyphicon glyphicon-picture" />;
		}
	}

	render() {
		return (
			<Transition
				component={false} // don't use a wrapping component
				enter={{ opacity: 1 }}
				leave={{ opacity: 0 }}
			>
				<div className="Thumbnail h-whitePanel h-paddingAll--sm" key="ThumbnailKey">
					<div className="Poster">
						<Link
							className="Poster__link"
							title={this.props.movie.original_title}
							alt={this.props.movie.original_title}
							to={`${routes.MOVIE_PAGE}/${this.props.movie.original_title}/id${this.props.movie.id}`}
						>
							{this.posterImage()}
						</Link>
					</div>

					<div className="Info">
						<p className="Info__data">
							<Link
								className="Info__title"
								title={this.props.movie.original_title}
								alt={this.props.movie.original_title}
								to={`${routes.MOVIE_PAGE}/${this.props.movie.original_title}/id${this.props.movie.id}`}
							>
								{this.props.movie.original_title}
							</Link>
							<span className="Info__voteHolder">
								<span className="Info__voteAverage">
									{this.props.movie.vote_average}
									<span id="rating" className="Info__voteStars glyphicon glyphicon-star">
										{' '}
									</span>
								</span>
							</span>
						</p>

						<p className="Info__meta">
							<span className="Info__releaseDate">
								<span className="Info__calendar glyphicon glyphicon-calendar"> </span>
								{this.props.movie.release_date}
							</span>
							<span className="Info__genres">{this.props.movie.genre}</span>
						</p>

						<p className="Info__overview">{this.props.movie.overview}</p>
						<p className="Info__viewMore">
							<Link
								className="Info__moreInfo"
								title={this.props.movie.original_title}
								alt={this.props.movie.original_title}
								to={`${routes.MOVIE_PAGE}/${this.props.movie.original_title}/id${this.props.movie.id}`}
							>
								More Info
							</Link>
						</p>
					</div>
				</div>
			</Transition>
		);
	}
}

const mapStateToProps = state => {
	return {
		currentPage: state.currentPageReducer.currentPage,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			getAllMovieData_action,
		},
		dispatch
	);
};

const ThumbnailComponent = connect(mapStateToProps, mapDispatchToProps)(_Thumbnail);

export default class Thumbnail extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<ThumbnailComponent {...this.props} />
			</Provider>
		);
	}
}
