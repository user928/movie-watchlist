import React from 'react';
import { bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { withRouter } from 'react-router-dom';
import {
	handleConfigurationApi_action,
	handleBrowse_action,
	handleSearch_action,
} from '../../redux/actionCreators/actionCreators';
import CurrentPageToggle from '../CurrentPageToggle/CurrentPageToggle';
import BrowseBox from '../BrowseBox/BrowseBox';
import SearchBox from '../SearchBox/SearchBox';
import Thumbnail from '../Thumbnail/Thumbnail';
import NotFound from '../NotFound/NotFound';
import InfoBox from '../InfoBox/InfoBox';
import { auth } from '../../firebase/firebase';
import * as routes from '../../routes';

class _App extends React.Component {
	constructor(props) {
		super(props);

		this.fetchSearchData = this.fetchSearchData.bind(this);
		this.fetchBrowseData = this.fetchBrowseData.bind(this);
		this.handleOnScroll = this.handleOnScroll.bind(this);
	}

	componentDidMount() {
		//fire this just first time (when app start)
		this.props.apiConfig_notStarted && this.props.handleConfigurationApi_action() && this.fetchBrowseData();
		//addEventListener
		window.addEventListener('scroll', this.handleOnScroll);

		auth.onAuthStateChanged(user => {
			if (!user) {
				const { history } = this.props;
				history.push(routes.SIGN_IN);
			}
		});
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleOnScroll);
	}

	handleOnScroll() {
		// http://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom
		let scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
		let scrollHeight =
			(document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
		let clientHeight = document.documentElement.clientHeight || window.innerHeight;
		let scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;

		if (scrolledToBottom) {
			if (this.props.currentPage === 'browse' && this.props.browseReducer.allowBrowseSearch) {
				this.fetchBrowseData();
			} else if (this.props.currentPage === 'search' && this.props.searchReducer.allowSearch) {
				this.fetchSearchData();
			}
		}
	}

	fetchBrowseData() {
		//this will send all values from sliders and selectDropDownReducer to handleBrowse_action api call (while increasing page number)
		this.props.handleBrowse_action(
			this.props.slidersReducer,
			this.props.reactSelect.value,
			this.props.browseReducer.nextPageNmbBrowse
		);
	}

	fetchSearchData() {
		//this will send all values from input to handleSearch_action api call (while increasing page number)
		this.props.handleSearch_action(this.props.searchValue, this.props.searchReducer.nextPageNmbSearch);
	}

	render() {
		return (
			<div className="container-fluid">
				<CurrentPageToggle />

				{this.props.currentPage === 'browse' ? (
					<BrowseBox fetchBrowseData={this.fetchBrowseData} />
				) : (
					<SearchBox fetchSearchData={this.fetchSearchData} />
				)}

				{this.props.browseReducer.error || this.props.searchReducer.error ? (
					<NotFound />
				) : (
					<div className="row h-marginB">
						<div className="col-xs-offset-1 col-xs-10 GridHolder GridHolder--twoCol GridHolder--gap2 h-marginT">
							{this.props.currentPage === 'browse'
								? this.props.browseReducer.apiResponse_browse.map((singleObject, index) => (
										<Thumbnail
											movie={singleObject}
											key={index}
											apiConfig_response={this.props.apiConfig_response}
											apiConfig_imagesSizes={this.props.apiConfig_imagesSizes}
											apiConfig_imagesBaseUrl={this.props.apiConfig_imagesBaseUrl}
										/>
									))
								: this.props.searchReducer.apiResponse_search.map((singleObject, index) => (
										<Thumbnail
											movie={singleObject}
											key={index}
											apiConfig_response={this.props.apiConfig_response}
											apiConfig_imagesSizes={this.props.apiConfig_imagesSizes}
											apiConfig_imagesBaseUrl={this.props.apiConfig_imagesBaseUrl}
										/>
									))}
						</div>

						{this.props.browseReducer.loading || this.props.searchReducer.loading ? (
							<InfoBox infoTxt="Loading ..." showLoader />
						) : !this.props.browseReducer.allowBrowseSearch || !this.props.searchReducer.allowSearch ? (
							<InfoBox infoTxt="No more results" />
						) : (
							false
						)}
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		searchReducer: state.searchReducer,
		browseReducer: state.browseReducer,
		apiConfig_notStarted: state.configurationApiReducer.apiConfig_notStarted,
		apiConfig_response: state.configurationApiReducer.apiConfig_response,
		apiConfig_imagesSizes: state.configurationApiReducer.apiConfig_imagesSizes,
		apiConfig_imagesBaseUrl: state.configurationApiReducer.apiConfig_imagesBaseUrl,
		currentPage: state.currentPageReducer.currentPage,
		slidersReducer: state.slidersReducer,
		reactSelect: state.selectDropDownReducer,
		searchValue: state.typingReducer.searchValue,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			handleConfigurationApi_action,
			handleBrowse_action,
			handleSearch_action,
		},
		dispatch
	);
};

const AppWithRouter = withRouter(_App);
const AppComponent = connect(mapStateToProps, mapDispatchToProps)(AppWithRouter);

export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<AppComponent {...this.props} />
			</Provider>
		);
	}
}
