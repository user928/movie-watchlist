import React from 'react';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import {
	clearSearchInput_action,
	clearSearchResults_action,
	showBrowse_action,
	showSearch_action,
	showList_action,
	clearBrowseResults_action,
} from '../../redux/actionCreators/actionCreators';
import { Link } from 'react-router-dom';
import * as routes from '../../routes'
import './CurrentPageToggle.css';

class _CurrentPageToggle extends React.Component {
	constructor(props) {
		super(props);
		this.switchToBrowsePage = this.switchToBrowsePage.bind(this);
		this.switchToSearchByNamePage = this.switchToSearchByNamePage.bind(this);
	}

	switchToBrowsePage() {
		this.props.clearBrowseResults_action();
		this.props.showBrowse_action();
	}

	switchToSearchByNamePage() {
		this.props.clearSearchResults_action();
		this.props.clearSearchInput_action();
		this.props.showSearch_action();
	}

	switchToListPage() {
		this.props.clearBrowseResults_action();
		this.props.clearSearchResults_action();
		this.props.clearSearchInput_action();
		this.props.showList_action();
	}

	render() {
		return (
			<div className="row">
				<div className="col-xs-offset-1 col-xs-10 h-marginT text-center">
					<div className={`activePage--${this.props.currentPage}`}>
						{/*Browse movies button*/}
						<Link
							to={routes.LANDING}
							className="Toggle__btn Toggle__btn--browse Button Button--secondary"
							onClick={() => this.switchToBrowsePage()}
						>
							Browse movies
						</Link>

						{/*Search movie by name button*/}
						<Link
							to={routes.LANDING}
							className="Toggle__btn Toggle__btn--search Button Button--secondary"
							onClick={() => this.switchToSearchByNamePage()}
						>
							Search by name
						</Link>

						{/*Movie list button*/}
						<Link
							to={routes.LIST_PAGE}
							className="Toggle__btn Toggle__btn--list Button Button--secondary"
							onClick={() => this.switchToListPage()}
						>
							Watchlist page
						</Link>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		currentPage: state.currentPageReducer.currentPage,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			clearSearchInput_action,
			clearSearchResults_action,
			showBrowse_action,
			showSearch_action,
			showList_action,
			clearBrowseResults_action,
		},
		dispatch
	);
};

const CurrentPageToggleComponent = connect(mapStateToProps, mapDispatchToProps)(_CurrentPageToggle);

export default class CurrentPageToggle extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<CurrentPageToggleComponent {...this.props} />
			</Provider>
		);
	}
}
