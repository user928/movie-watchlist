import React from 'react';
import { auth, db } from '../../firebase';
import * as routes from '../../routes';
import { Link, withRouter } from 'react-router-dom';

const updateByPropertyName = (propertyName, value) => () => ({
	[propertyName]: value,
});

const INITIAL_STATE = {
	username: '',
	email: '',
	passwordOne: '',
	passwordTwo: '',
	error: null,
};

class SignUpPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...INITIAL_STATE };
	}

	onSubmit = event => {
		const { username, email, passwordOne } = this.state;

		const { history } = this.props;

		auth
			.doCreateUserWithEmailAndPassword(email, passwordOne)
			.then(authUser => {
				db.doCreateUser(authUser.uid, username, email);
				this.setState({ ...INITIAL_STATE }, () => {
					history.push(routes.LANDING);
				});
			})
			.catch(error => {
				this.setState(updateByPropertyName('error', error));
			});

		event.preventDefault();
	};

	render() {
		const { username, email, passwordOne, passwordTwo, error } = this.state;

		const isInvalid = passwordOne !== passwordTwo || passwordOne === '' || username === '' || email === '';

		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-offset-4 col-md-4 col-xs-offset-2 col-xs-8">
						<h1 className="text-center h-marginB">Create new account</h1>
						<form onSubmit={this.onSubmit}>
							<input
								className="Input"
								value={username}
								onChange={event => this.setState(updateByPropertyName('username', event.target.value))}
								type="text"
								placeholder="Full Name"
                                autoComplete="true"
							/>
							<input
								className="Input"
								value={email}
								onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
								type="text"
								placeholder="Email Address"
                                autoComplete="true"
							/>
							<input
								className="Input"
								value={passwordOne}
								onChange={event =>
									this.setState(updateByPropertyName('passwordOne', event.target.value))}
								type="password"
								placeholder="Password"
                                autoComplete="true"
							/>
							<input
								className="Input"
								value={passwordTwo}
								onChange={event =>
									this.setState(updateByPropertyName('passwordTwo', event.target.value))}
								type="password"
								placeholder="Confirm Password"
                                autoComplete="true"
							/>
							<button
								className="Button Button--primary h-marginB center-block"
								disabled={isInvalid}
								type="submit"
							>
								Sign Up
							</button>

							{error && <p className="text-center h-errorBox">{error.message}</p>}
						</form>
						<p className="text-center">
							Already have an account? <Link to={routes.SIGN_IN}>Sign In</Link> instead.
						</p>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(SignUpPage);
