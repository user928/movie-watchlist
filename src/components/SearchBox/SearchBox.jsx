import React from 'react';
import Transition from 'react-motion-ui-pack';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import { handleTyping_action, clearSearchResults_action } from '../../redux/actionCreators/actionCreators';

class _SearchBox extends React.Component {
	constructor(props) {
		super(props);
		this.handleEnterKey = this.handleEnterKey.bind(this);
		this.fetchSearchAndClear = this.fetchSearchAndClear.bind(this);
	}

	fetchSearchAndClear() {
		this.props.clearSearchResults_action();
		//make sure we clear and reset all before performing new search
		setTimeout(() => {
			this.props.fetchSearchData();
		}, 0);
	}

	handleEnterKey(e) {
		if (e.key === 'Enter') {
			this.fetchSearchAndClear();
		}
	}

	render() {
		return (
			<Transition
				component={false} // don't use a wrapping component
				enter={{ opacity: 1 }}
				leave={{ opacity: 0 }}
			>
				<div className="row" key="SearchBoxKey">
					<div className="SearchBox col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6">
						<div className="h-marginT h-flex">
							<input
								type="text"
								placeholder="Type movie name here to search..."
								className="form-control"
								value={this.props.searchValue}
								onChange={e => this.props.handleTyping_action(e)}
								onKeyPress={e => this.handleEnterKey(e)}
							/>

							<button
								className="Button Button--primary Button--searchBox"
								disabled={this.props.searchDisable}
								onClick={e =>
									//disable search if nothing is typed into box, else perform fetchSearchData
									this.props.searchDisable ? e.preventDefault() : this.fetchSearchAndClear()}
							>
								Search
							</button>
						</div>
					</div>
				</div>
			</Transition>
		);
	}
}

const mapStateToProps = state => {
	return {
		searchValue: state.typingReducer.searchValue,
		searchDisable: state.typingReducer.searchDisable,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			handleTyping_action,
			clearSearchResults_action,
		},
		dispatch
	);
};

const SearchBoxComponent = connect(mapStateToProps, mapDispatchToProps)(_SearchBox);

export default class SearchBox extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<SearchBoxComponent {...this.props} />
			</Provider>
		);
	}
}
