import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import { auth } from '../../firebase';
import * as routes from '../../routes';

const updateByPropertyName = (propertyName, value) => () => ({
	[propertyName]: value,
});

const INITIAL_STATE = {
	email: '',
	error: null,
	isResetSuccessful: false,
};

class PasswordForget extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...INITIAL_STATE };
	}

	onSubmit = event => {
		const { email } = this.state;

		auth
			.doPasswordReset(email)
			.then(() => {
				this.setState({
					isResetSuccessful: true,
				});
			})
			.catch(error => {
				this.setState(updateByPropertyName('error', error));
			});

		event.preventDefault();
	};

	render() {
		const { email, error } = this.state;

		const isInvalid = email === '';

		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-offset-4 col-md-4 col-xs-offset-2 col-xs-8">
						<h1 className="text-center h-marginB">Reset password</h1>
						<form onSubmit={this.onSubmit}>
							<input
								className="Input"
								value={this.state.email}
								onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
								type="text"
								placeholder="Email Address"
								autoComplete="true"
							/>
							{this.state.isResetSuccessful ? (
								<p className="text-center h-txtBold">Check your email for password reset link.</p>
							) : (
								<button
									className="Button Button--primary h-marginB center-block"
									disabled={isInvalid}
									type="submit"
								>
									Reset My Password
								</button>
							)}

							{error && <p className="text-center h-errorBox">{error.message}</p>}
						</form>
						<p className="text-center">
							Don't have an account? <Link to={routes.SIGN_UP}>Sign Up</Link> instead.
						</p>
						<p className="text-center">
							Return to <Link to={routes.SIGN_IN}>Sign In</Link> page.
						</p>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(PasswordForget);
