import React from 'react';
import Transition from 'react-motion-ui-pack';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import {
	sendGenresData_action,
	selectChange_action,
	applyButtonDisable_action,
} from '../../redux/actionCreators/actionCreators';
import Select from 'react-select';
import '../../css/vendor/react-select.css';
import Sliders from '../Sliders/Sliders';

class _BrowseBox extends React.Component {
	constructor(props) {
		super(props);
		this.handleApply = this.handleApply.bind(this);
	}

	componentDidMount() {
		//this will send all movie genres data to selectDropDownReducer so we can show it inside dropDown
		this.props.sendGenresData_action(this.props.configurationApiGenres);

		//this will send all values from sliders and selectDropDownReducer to handleBrowse_action api call (while increasing page number)
		this.props.fetchBrowseData();
	}

	handleApply() {
		//this will send all values from sliders and selectDropDownReducer to handleBrowse_action api call (while increasing page number)
		this.props.fetchBrowseData();
		//this will disable apply button after user click on it
		this.props.applyButtonDisable_action(true);
	}

	render() {
		return (
			<Transition
				component={false} // don't use a wrapping component
				enter={{ opacity: 1 }}
				leave={{ opacity: 0 }}
			>
				<div className="row h-zIndex-15" key="BrowseBoxKey">
					<div className="col-xs-offset-1 col-xs-10">
						<Sliders />
					</div>

					<div className="col-xs-6 col-xs-offset-3 h-marginT">
						<Select
							multi
							simpleValue
							value={this.props.reactSelect.value}
							options={this.props.reactSelect.allGenres}
							onChange={this.props.selectChange_action}
							placeholder="Filter by genres ..."
						/>
					</div>

					<div className="col-xs-offset-1 col-xs-10 text-center h-marginT">
						<button
							className="Button Button--primary"
							disabled={this.props.applyButtonDisable}
							onClick={this.handleApply}
						>
							Apply
						</button>
					</div>
				</div>
			</Transition>
		);
	}
}

const mapStateToProps = state => {
	return {
		applyButtonDisable: state.applyButtonReducer.disable,
		slidersReducer: state.slidersReducer,
		reactSelect: state.selectDropDownReducer,
		configurationApiGenres: state.configurationApiReducer.allGenres,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			sendGenresData_action,
			applyButtonDisable_action,
			selectChange_action,
		},
		dispatch
	);
};

const BrowseBoxComponent = connect(mapStateToProps, mapDispatchToProps)(_BrowseBox);

export default class BrowseBox extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<BrowseBoxComponent {...this.props} />
			</Provider>
		);
	}
}
