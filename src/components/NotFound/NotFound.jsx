import React from 'react';

class NotFound extends React.Component {
	render() {
		return (
			<div className="row">
				<div className="col-xs-offset-1 col-xs-10">
					<h1>Not Found !!!</h1>
				</div>
			</div>
		)
	}
}

export default NotFound;