import React from 'react';
import './infoBox.css';

export default class Loader extends React.Component {
	render() {
		return (
			<div className="row">
				<div className="col-xs-12 h-marginT text-center">
					<h2 className={this.props.showLoader ? 'InfoBox__txt InfoBox__txt--animation' : 'InfoBox__txt'}>
						{this.props.showLoader && (
							<div className="loader">
								<svg className="circular" viewBox="25 25 50 50">
									<circle
										className="path"
										cx="50"
										cy="50"
										r="20"
										fill="none"
										strokeWidth="2"
										strokeMiterlimit="10"
									/>
								</svg>
							</div>
						)}

						{this.props.infoTxt}
					</h2>
				</div>
			</div>
		);
	}
}
