import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import { auth } from '../../firebase';
import * as routes from '../../routes';

const updateByPropertyName = (propertyName, value) => () => ({
	[propertyName]: value,
});

const INITIAL_STATE = {
	email: '',
	password: '',
	error: null,
};

class SignInPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...INITIAL_STATE };
	}

	onSubmit = event => {
		const { email, password } = this.state;

		const { history } = this.props;

		auth
			.doSignInWithEmailAndPassword(email, password)
			.then(() => {
				this.setState({ ...INITIAL_STATE }, () => {
					history.push(routes.LANDING);
				});
			})
			.catch(error => {
				this.setState(updateByPropertyName('error', error));
			});

		event.preventDefault();
	};

	render() {
		const { email, password, error } = this.state;

		const isInvalid = password === '' || email === '';

		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-offset-4 col-md-4 col-xs-offset-2 col-xs-8">
						<h1 className="text-center h-marginB">Log in</h1>
						<form onSubmit={this.onSubmit}>
							<input
								className="Input"
								value={email}
								onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
								type="text"
								placeholder="Email Address"
								autoComplete="true"
							/>
							<input
								className="Input"
								value={password}
								onChange={event => this.setState(updateByPropertyName('password', event.target.value))}
								type="password"
								placeholder="Password"
								autoComplete="true"
							/>
							<button
								className="Button Button--primary h-marginB center-block"
								disabled={isInvalid}
								type="submit"
							>
								Sign In
							</button>

							{error && <p className="text-center h-errorBox">{error.message}</p>}
						</form>
						<p className="text-center">
							Forgot your password? <Link to={routes.PASSWORD_FORGET}>Reset password</Link>
						</p>

						<p className="text-center">
							Don't have an account? <Link to={routes.SIGN_UP}>Sign Up</Link> instead.
						</p>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(SignInPage);
