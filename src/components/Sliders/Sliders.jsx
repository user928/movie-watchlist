import React from 'react';
import InputRange from 'react-input-range';
import { connect, Provider } from 'react-redux';
import { store } from '../../redux/store';
import { bindActionCreators } from 'redux';
import { sliders_actions } from '../../redux/actionCreators/actionCreators';

import './sliders.css';

class _Sliders extends React.Component {
	render() {
		return (
			<div className="h-marginT GridHolder GridHolder--threeCol GridHolder--gap2">
				<div className="h-whitePanel h-paddingAll">
					<p className="input-range__sliderTitle">Movie rating</p>
					<InputRange
						minValue={1}
						maxValue={10}
						formatLabel={value => `${value}`}
						onChange={value => this.props.sliders_actions(value, 'setSliderRating')}
						onChangeComplete={value => this.props.sliders_actions(value, 'setSliderRating')}
						value={this.props.slidersReducer.rating}
					/>
				</div>

				<div className="h-whitePanel h-paddingAll">
					<p className="input-range__sliderTitle">Year range</p>
					<InputRange
						minValue={2010}
						maxValue={new Date().getFullYear()}
						formatLabel={value => `${value}`}
						onChange={value => this.props.sliders_actions(value, 'setSliderYear')}
						onChangeComplete={value => this.props.sliders_actions(value, 'setSliderYear')}
						value={this.props.slidersReducer.year}
					/>
				</div>

				<div className="h-whitePanel h-paddingAll">
					<p className="input-range__sliderTitle">Movie duration</p>
					<InputRange
						minValue={40}
						maxValue={210}
						step={10}
						formatLabel={value => `${value}`}
						onChange={value => this.props.sliders_actions(value, 'setSliderDuration')}
						onChangeComplete={value => this.props.sliders_actions(value, 'setSliderDuration')}
						value={this.props.slidersReducer.duration}
					/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		slidersReducer: state.slidersReducer,
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			sliders_actions,
		},
		dispatch
	);
};

const SlidersComponent = connect(mapStateToProps, mapDispatchToProps)(_Sliders);

export default class Sliders extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<SlidersComponent {...this.props} />
			</Provider>
		);
	}
}
