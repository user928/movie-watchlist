import { db } from './firebase';

// User API

export const doCreateUser = (id, username, email) => {
	db.ref(`users/${id}`).set({
		username,
		email,
	});
};

export const onceGetUsers = () => db.ref('users').once('value');

// Movie list API

export const getMovieList = id => db.ref(`users/${id}/movie-list`).once('value');

export const addMovieToList = (id, movieTitle, movieDataAll) => {
	db.ref(`users/${id}/movie-list/${movieTitle}`).set({ movieDataAll: movieDataAll });
};

export const removeMovieFromList = (id, movieTitle) => {
	db.ref(`users/${id}/movie-list/${movieTitle}`).remove();
};
