import * as firebase from 'firebase';

const config = {
	apiKey: 'AIzaSyD6erwmiZM5X_jzFqMhZawTEuskjyBmrrg',
	authDomain: 'movie-watch-list-react.firebaseapp.com',
	databaseURL: 'https://movie-watch-list-react.firebaseio.com',
	projectId: 'movie-watch-list-react',
	storageBucket: '',
	messagingSenderId: '387274649359',
};

if (!firebase.apps.length) {
	firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export {
	db,
	auth,
};
