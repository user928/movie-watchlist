import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import * as routes from './routes';

import App from './components/App/App';
import MoviePage from './components/MoviePage/MoviePage';
import ListPage from './components/ListPage/ListPage';
import SignIn from './components/SignIn/SignIn';
import SignUp from './components/SignUp/SignUp';
import PasswordForget from './components/PasswordForget/PasswordForget';
import NotFound from './components/NotFound/NotFound';

import './css/style.css';
import './css/vendor/bootstrap3.css';
import './css/base.css';
import './css/shared.css';

const Root = () => {
	return (
		<Router>
			<div className="app">
				<Switch>
					<Route exact path={routes.LANDING} component={() => <App />} />
					<Route path={routes.MOVIE_PAGE} component={() => <MoviePage />} />
					<Route exact path={routes.LIST_PAGE} component={() => <ListPage />} />
					<Route exact path={routes.SIGN_IN} component={() => <SignIn />} />
					<Route exact path={routes.SIGN_UP} component={() => <SignUp />} />
					<Route exact path={routes.PASSWORD_FORGET} component={() => <PasswordForget />} />
					<Route component={() => <NotFound />} />
				</Switch>
			</div>
		</Router>
	);
};

ReactDOM.render(<Root />, document.querySelector('#main'));
