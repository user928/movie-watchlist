import { actionTypes } from '../constants/actionTypes';

const initialState = {
	movieDataAll: {
		movieData: {},
		moviePoster: '',
	},
};
export default function moviePageReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.RECEIVED_ALL_MOVIE_DATA:
			return {
				...state,
				movieDataAll: {
					movieData: action.payload.data,
					moviePoster: action.payloadPoster,
					movieDataYTkey: action.payload.data.videos.results[0].key,
				},
			};
		case actionTypes.RECEIVED_ALL_MOVIE_DATA_ERROR:
			return {
				...state,
				movieDataYTkeyError: action.payload.data,
			};
		default:
			return state;
	}
}
