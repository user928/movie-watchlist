import { actionTypes } from '../constants/actionTypes';

const initialState = {
	disable: true,
};

export default function applyButtonReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.APPLY_BUTTON_DISABLE:
			return {
				...state,
				disable: action.payload,
			};
		default:
			return state;
	}
}
