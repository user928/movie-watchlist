import { actionTypes } from '../constants/actionTypes';

const initialState = {
	apiResponse_search: [],
	nextPageNmbSearch: 1,
	allowSearch: true,
};

export default function searchReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_SEARCH_RESULTS:
			return {
				...state,
				apiResponse_search: [],
				nextPageNmbSearch: 1,
				allowSearch: true,
				loading: false,
				error: false,
			};
		case actionTypes.STOP_SEARCH:
			return {
				...state,
				allowSearch: false,
				loading: false,
				error: false,
			};
		case actionTypes.REQUEST_SEARCH:
			return {
				...state,
				nextPageNmbSearch: state.nextPageNmbSearch + 1,
				loading: true,
				error: false,
			};
		case actionTypes.RECEIVE_SEARCH_SUCESS:
			return {
				...state,
				apiResponse_search: [...state.apiResponse_search, ...action.payload.results],
				loading: false,
				error: false,
			};
		case actionTypes.RECEIVE_SEARCH_ERROR:
			return {
				...state,
				apiResponse_search: action.payload.results,
				loading: false,
				error: true,
			};
		default:
			return state;
	}
}
