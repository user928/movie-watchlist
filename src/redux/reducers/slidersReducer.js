import { actionTypes } from '../constants/actionTypes';

const initialState = {
	rating: {
		min: 6,
		max: 10,
	},
	year: {
		min: 2012,
		max: new Date().getFullYear(),
	},
	duration: {
		min: 60,
		max: 90,
	},
};

export default function slidersReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.SET_SLIDER_RATING:
			return {
				...state,
				rating: {
					min: action.payload.min,
					max: action.payload.max,
				},
			};
		case actionTypes.SET_SLIDER_YEAR:
			return {
				...state,
				year: {
					min: action.payload.min,
					max: action.payload.max,
				},
			};
		case actionTypes.SET_SLIDER_DURATION:
			return {
				...state,
				duration: {
					min: action.payload.min,
					max: action.payload.max,
				},
			};
		default:
			return state;
	}
}
