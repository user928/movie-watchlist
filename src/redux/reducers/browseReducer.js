import { actionTypes } from '../constants/actionTypes';

const initialState = {
	apiResponse_browse: [],
	nextPageNmbBrowse: 1,
	allowBrowseSearch: true,
};

export default function browseReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_BROWSE_RESULTS:
			return {
				...state,
				apiResponse_browse: [],
				nextPageNmbBrowse: 1,
				allowBrowseSearch: true,
				loading: false,
				error: false,
			};
		case actionTypes.STOP_BROWSE:
			return {
				...state,
				allowBrowseSearch: false,
				loading: false,
				error: false,
			};
		case actionTypes.REQUEST_BROWSE:
			return {
				...state,
				nextPageNmbBrowse: state.nextPageNmbBrowse + 1,
				loading: true,
				error: false,
			};
		case actionTypes.RECEIVE_BROWSE_SUCESS:
			return {
				...state,
				apiResponse_browse: [...state.apiResponse_browse, ...action.payload.results],
				loading: false,
				error: false,
			};
		case actionTypes.RECEIVE_BROWSE_ERROR:
			return {
				...state,
				apiResponse_browse: action.payload.results,
				loading: false,
				error: true,
			};
		default:
			return state;
	}
}
