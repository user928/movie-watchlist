import { actionTypes } from "../constants/actionTypes"

const initialState = {
	searchValue: '',
	searchDisable: true
};
export default function typingReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.HANDLE_TYPING:
			if (action.payload.length > 0) {
				return {
					...state,
					searchValue: action.payload,
					searchDisable: false,
				};
				//disable search if nothing is typed into box
			} else {
				return {
					searchValue: action.payload,
					searchDisable: true,
				};
			}
		case actionTypes.CLEAR_SEARCH_INPUT:
			return {
				searchValue: '',
				searchDisable: true
			};
		default:
			return state
	}
}

