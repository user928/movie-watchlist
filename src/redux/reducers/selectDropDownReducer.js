import { actionTypes } from '../constants/actionTypes';

const initialState = {
	value: [],
};

export default function selectDropDownReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.SEND_GENRES_DATA:
			return {
				...state,
				allGenres: action.payload,
			};
		case actionTypes.REACT_SELECT_CHANGE:
			return {
				...state,
				value: action.payload,
			};
		default:
			return state;
	}
}
