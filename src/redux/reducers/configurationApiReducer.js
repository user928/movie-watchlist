import { actionTypes } from "../constants/actionTypes"

const initialState = {
    //this reducer should fire only once per app load so we use "apiConfig_notStarted"
    apiConfig_notStarted: true,
    apiConfig_response: [],
	apiConfig_imagesBaseUrl: [],
	apiConfig_imagesSizes: [],
    allGenres: [
        {
            "id": 28,
            "name": "Action"
        },
        {
            "id": 12,
            "name": "Adventure"
        },
        {
            "id": 16,
            "name": "Animation"
        },
        {
            "id": 35,
            "name": "Comedy"
        },
        {
            "id": 80,
            "name": "Crime"
        },
        {
            "id": 99,
            "name": "Documentary"
        },
        {
            "id": 18,
            "name": "Drama"
        },
        {
            "id": 10751,
            "name": "Family"
        },
        {
            "id": 14,
            "name": "Fantasy"
        },
        {
            "id": 36,
            "name": "History"
        },
        {
            "id": 27,
            "name": "Horror"
        },
        {
            "id": 10402,
            "name": "Music"
        },
        {
            "id": 9648,
            "name": "Mystery"
        },
        {
            "id": 10749,
            "name": "Romance"
        },
        {
            "id": 878,
            "name": "Science Fiction"
        },
        {
            "id": 10770,
            "name": "TV Movie"
        },
        {
            "id": 53,
            "name": "Thriller"
        },
        {
            "id": 10752,
            "name": "War"
        },
        {
            "id": 37,
            "name": "Western"
        }
    ],
};
export default function configurationApiReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.REQUEST_CONFIGURATION_API:
			return {
				...state,
				loading: true,
				error: false,
			};
		case actionTypes.RECEIVE_CONFIGURATION_API:
			return {
				...state,
				apiConfig_response: action.payload.data,
				apiConfig_imagesBaseUrl: action.payload.data.images.base_url,
				apiConfig_imagesSizes: action.payload.data.images.backdrop_sizes,
				loading: false,
				error: false,
				//this reducer should fire only once per app load so we use "apiConfig_notStarted"
				apiConfig_notStarted: false
			};
		case actionTypes.RECEIVE_CONFIGURATION_API_ERROR:
			return {
				...state,
				apiConfig_response: action.payload.data,
				loading: false,
				error: true,
			};
		default:
			return state
	}
}