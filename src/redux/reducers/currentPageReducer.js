import { actionTypes } from "../constants/actionTypes"

const initialState = {
    currentPage: 'browse',
};

export default function currentPageReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.SHOW_BROWSE:
            return {
                ...state,
                currentPage: 'browse',
			};
		case actionTypes.SHOW_SEARCH:
            return {
                ...state,
                currentPage: 'search',
			};
		case actionTypes.SHOW_LIST:
            return {
                ...state,
                currentPage: 'list',
			};
		default:
			return state
	}
}

