import { combineReducers } from 'redux';
import searchReducer from './reducers/searchReducer';
import browseReducer from './reducers/browseReducer';
import typingReducer from './reducers/typingReducer';
import configurationApiReducer from './reducers/configurationApiReducer';
import movieDataReducer from './reducers/movieDataReducer';
import slidersReducer from './reducers/slidersReducer';
import currentPageReducer from './reducers/currentPageReducer';
import selectDropDownReducer from './reducers/selectDropDownReducer';
import applyButtonReducer from './reducers/applyButtonReducer';

export default combineReducers({
	searchReducer,
	browseReducer,
	typingReducer,
	configurationApiReducer,
	movieDataReducer,
	slidersReducer,
	currentPageReducer,
	selectDropDownReducer,
	applyButtonReducer,
});
