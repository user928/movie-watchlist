import axios from 'axios';
import { actionTypes } from '../constants/actionTypes';

//handleTyping_action
export function handleTyping_action(e) {
	return {
		type: actionTypes.HANDLE_TYPING,
		payload: e.target.value,
	};
}

//handleSearch_action
//https://developers.themoviedb.org/3/search/search-movies
const API_key = 'e13c197a08f66430e4fb6e4a495b86c3';
const API_url_base = `https://api.themoviedb.org/3/`;
const API_url_search = `${API_url_base}search/movie?api_key=${API_key}`;

export function handleSearch_action(searchValue, pageNumber) {
	const API_ulr = `${API_url_search}&query=${searchValue}&page=${pageNumber}`;
	return function(dispatch) {
		dispatch({
			type: actionTypes.REQUEST_SEARCH,
		});

		return axios
			.get(API_ulr)
			.then(response => {
				response.data.page > response.data.total_pages
					? dispatch({
							type: actionTypes.STOP_SEARCH,
						})
					: response.data.results.length === 0
						? dispatch({
								type: actionTypes.RECEIVE_SEARCH_ERROR,
								payload: response,
							})
						: dispatch({
								type: actionTypes.RECEIVE_SEARCH_SUCESS,
								payload: response.data,
							});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVE_SEARCH_ERROR,
					payload: response,
				});
			});
	};
}

//handleBrowse_action
//for browse filters we use npm components: react-component.github.io/slider/ and github.com/i-like-robots/react-tags
const API_url_browse = `${API_url_base}discover/movie?api_key=${API_key}`;

export function handleBrowse_action(slidersReducerData, genres, pageNumber) {
	const API_url_browse_parameters = `&vote_count.gte=30&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${pageNumber}&primary_release_date.gte=${slidersReducerData
		.year.min}&primary_release_date.lte=${slidersReducerData.year.max}&vote_average.gte=${slidersReducerData.rating
		.min}&vote_average.lte=${slidersReducerData.rating.max}&with_runtime.gte=${slidersReducerData.duration
		.min}&with_runtime.lte=${slidersReducerData.duration.max}&with_genres=${genres}`;
	const API_ulr = `${API_url_browse}${API_url_browse_parameters}`;
	return function(dispatch) {
		dispatch({
			type: actionTypes.REQUEST_BROWSE,
		});

		return axios
			.get(API_ulr)
			.then(response => {
				response.data.page > response.data.total_pages
					? dispatch({
							type: actionTypes.STOP_BROWSE,
						})
					: response.data.results.length === 0
						? dispatch({
								type: actionTypes.RECEIVE_BROWSE_ERROR,
								payload: response,
							})
						: dispatch({
								type: actionTypes.RECEIVE_BROWSE_SUCESS,
								payload: response.data,
							});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVE_BROWSE_ERROR,
					payload: response,
				});
			});
	};
}

//handleConfigurationApi_action
const API_url_configuration_api = `${API_url_base}configuration?api_key=${API_key}`;

export function handleConfigurationApi_action() {
	return function(dispatch) {
		return axios
			.get(API_url_configuration_api)
			.then(
				dispatch({
					type: actionTypes.REQUEST_CONFIGURATION_API,
				})
			)
			.then(response => {
				dispatch({
					type: actionTypes.RECEIVE_CONFIGURATION_API,
					payload: response,
				});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVE_CONFIGURATION_API_ERROR,
					payload: response,
				});
			});
	};
}

//clearSearchInput_action
export function clearSearchInput_action() {
	return {
		type: actionTypes.CLEAR_SEARCH_INPUT,
	};
}

//clearSearchResults_action
export function clearSearchResults_action() {
	return {
		type: actionTypes.CLEAR_SEARCH_RESULTS,
	};
}

//clearBrowseResults_action
export function clearBrowseResults_action() {
	return {
		type: actionTypes.CLEAR_BROWSE_RESULTS,
	};
}

//setSliderRating_action
export function setSliderRating_action(value) {
	return {
		type: actionTypes.SET_SLIDER_RATING,
		payload: value,
	};
}
//setSliderYear_action
export function setSliderYear_action(value) {
	return {
		type: actionTypes.SET_SLIDER_YEAR,
		payload: value,
	};
}

//setSliderDuration_action
export function setSliderDuration_action(value) {
	return {
		type: actionTypes.SET_SLIDER_DURATION,
		payload: value,
	};
}

//sliders_action this will disable/enable apply button, clear all api data and then fire corresponding slider action
export function sliders_actions(value, sliderType) {
	return function(dispatch) {
		dispatch(clearBrowseResults_action());
		dispatch(applyButtonDisable_action(false));

		// fire corresponding slider action
		if (sliderType === 'setSliderRating') {
			dispatch(setSliderRating_action(value));
		} else if (sliderType === 'setSliderYear') {
			dispatch(setSliderYear_action(value));
		} else if (sliderType === 'setSliderDuration') {
			dispatch(setSliderDuration_action(value));
		}
	};
}

//applyButtonDisable_action
export function applyButtonDisable_action(value) {
	return {
		type: actionTypes.APPLY_BUTTON_DISABLE,
		payload: value,
	};
}

//getAllMovieData_action
export function getAllMovieData_action(value, posterData) {
	return function(dispatch) {
		return axios
			.get(`http://api.themoviedb.org/3/movie/${value}?api_key=${API_key}&append_to_response=videos`)
			.then(response => {
				dispatch({
					type: actionTypes.RECEIVED_ALL_MOVIE_DATA,
					payload: response,
					payloadPoster: posterData,
				});
			})
			.catch(response => {
				dispatch({
					type: actionTypes.RECEIVED_ALL_MOVIE_DATA_ERROR,
					payload: response,
				});
			});
	};
}

//showBrowse_action
export function showBrowse_action() {
	return {
		type: actionTypes.SHOW_BROWSE,
	};
}

//showSearch_action
export function showSearch_action() {
	return {
		type: actionTypes.SHOW_SEARCH,
	};
}

//showList_action
export function showList_action() {
	return {
		type: actionTypes.SHOW_LIST,
	};
}

//sendGenresData_action
export function sendGenresData_action(data) {
	//convert genres object to fit reactSelect component
	const allGenres = data.map(singleItem => ({
		value: singleItem.id.toString(),
		label: singleItem.name,
	}));

	return {
		type: actionTypes.SEND_GENRES_DATA,
		payload: allGenres,
	};
}

//reactSelectChange_action
export function reactSelectChange_action(data) {
	return {
		type: actionTypes.REACT_SELECT_CHANGE,
		payload: data,
	};
}

//selectChange_action this will disable/enable apply button, clear all api data and then fire reactSelectChange_action action
export function selectChange_action(data) {
	return function(dispatch) {
		dispatch(clearBrowseResults_action());
		dispatch(applyButtonDisable_action(false));
		dispatch(reactSelectChange_action(data));
	};
}
