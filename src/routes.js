export const LANDING = '/';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const PASSWORD_FORGET = '/password-forget';
export const MOVIE_PAGE = '/movie-page';
export const LIST_PAGE = '/list-page';
